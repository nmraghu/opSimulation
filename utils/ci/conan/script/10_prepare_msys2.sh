#!/bin/bash

################################################################################
# Copyright (c) 2021 ITK Engineering GmbH
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script prepares the MSYS2 environment
################################################################################


## manual installation of compatible versions from https://repo.msys2.org/mingw/mingw64/
# none


## automatic installation of latest versions
yes | pacman -S mingw-w64-x86_64-gcc
yes | pacman -S mingw-w64-x86_64-gdb
yes | pacman -S mingw-w64-x86_64-cmake
yes | pacman -S mingw-w64-x86_64-doxygen
yes | pacman -S mingw-w64-x86_64-qt5
yes | pacman -S mingw-w64-x86_64-qt5-debug
yes | pacman -S mingw-w64-x86_64-make
yes | pacman -S mingw-w64-x86_64-boost
yes | pacman -S mingw-w64-x86_64-protobuf
yes | pacman -S mingw-w64-x86_64-gtest
yes | pacman -S mingw-w64-x86_64-ccache
yes | pacman -S mingw-w64-x86_64-graphviz
yes | pacman -S mingw-w64-x86_64-python
yes | pacman -S mingw-w64-x86_64-python-pip
yes | pacman -S mingw-w64-x86_64-python-sphinx
yes | pacman -S git


## optional
# yes | pacman -S dos2unix
# yes | pacman -S make
# yes | pacman -S diffutils
# yes | pacman -S patch
# yes | pacman -S mingw-w64-x86_64-ag
# yes | pacman -S zlib-devel
# yes | pacman -S mingw-w64-x86_64-enchant


## list results
pacman -Qe
