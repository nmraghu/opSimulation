#!/bin/bash

################################################################################
# Copyright (c) 2021 ITK Engineering GmbH
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script prepares the MSYS2 environment
################################################################################


## manual installation of compatible versions from https://repo.msys2.org/mingw/mingw64/
wget https://repo.msys2.org/mingw/mingw64/mingw-w64-x86_64-gcc-libs-10.2.0-6-any.pkg.tar.zst
wget https://repo.msys2.org/mingw/mingw64/mingw-w64-x86_64-gcc-10.2.0-6-any.pkg.tar.zst
wget https://repo.msys2.org/mingw/mingw64/mingw-w64-x86_64-gdb-10.1-2-any.pkg.tar.zst
wget https://repo.msys2.org/mingw/mingw64/mingw-w64-x86_64-cmake-3.19.2-1-any.pkg.tar.zst
wget https://repo.msys2.org/mingw/mingw64/mingw-w64-x86_64-doxygen-1.8.20-1-any.pkg.tar.zst
wget https://repo.msys2.org/mingw/mingw64/mingw-w64-x86_64-qt5-5.15.2-5-any.pkg.tar.zst # yes | pacman -S mingw-w64-x86_64-qt5
wget https://repo.msys2.org/mingw/mingw64/mingw-w64-x86_64-qt5-debug-5.15.2-5-any.pkg.tar.zst # yes | pacman -S mingw-w64-x86_64-qt5-debug
yes | pacman -U mingw-w64-x86_64-gcc-libs-10.2.0-6-any.pkg.tar.zst # yes | pacman -S mingw-w64-x86_64-gcc
yes | pacman -U mingw-w64-x86_64-gcc-10.2.0-6-any.pkg.tar.zst # yes | pacman -S mingw-w64-x86_64-gcc
yes | pacman -U mingw-w64-x86_64-gdb-10.1-2-any.pkg.tar.zst # yes | pacman -S mingw-w64-x86_64-gdb
yes | pacman -U mingw-w64-x86_64-cmake-3.19.2-1-any.pkg.tar.zst # yes | pacman -S mingw-w64-x86_64-cmake
yes | pacman -U mingw-w64-x86_64-doxygen-1.8.20-1-any.pkg.tar.zst # yes | pacman -S mingw-w64-x86_64-doxygen
yes | pacman -U mingw-w64-x86_64-qt5-5.15.2-5-any.pkg.tar.zst
yes | pacman -U mingw-w64-x86_64-qt5-debug-5.15.2-5-any.pkg.tar.zst
# rm mingw-w64-x86_64-gcc-libs-10.2.0-6-any.pkg.tar.zst
# rm mingw-w64-x86_64-gcc-10.2.0-6-any.pkg.tar.zst
# rm mingw-w64-x86_64-gdb-10.1-2-any.pkg.tar.zst
# rm mingw-w64-x86_64-cmake-3.19.2-1-any.pkg.tar.zst
# rm mingw-w64-x86_64-doxygen-1.8.20-1-any.pkg.tar.zst
# rm mingw-w64-x86_64-qt5-5.15.2-5-any.pkg.tar.zst
# rm mingw-w64-x86_64-qt5-debug-5.15.2-5-any.pkg.tar.zst


## automatic installation of latest versions
yes | pacman -S mingw-w64-x86_64-make
yes | pacman -S mingw-w64-x86_64-boost
yes | pacman -S mingw-w64-x86_64-protobuf
yes | pacman -S mingw-w64-x86_64-gtest
yes | pacman -S mingw-w64-x86_64-ccache
yes | pacman -S mingw-w64-x86_64-graphviz
yes | pacman -S mingw-w64-x86_64-python
yes | pacman -S mingw-w64-x86_64-python-pip
yes | pacman -S mingw-w64-x86_64-python-sphinx
yes | pacman -S git


## optional
# yes | pacman -S dos2unix
# yes | pacman -S make
# yes | pacman -S diffutils
# yes | pacman -S patch
# yes | pacman -S mingw-w64-x86_64-ag
# yes | pacman -S zlib-devel
# yes | pacman -S mingw-w64-x86_64-enchant


## list results
pacman -Qe
